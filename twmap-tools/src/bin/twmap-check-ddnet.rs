use twmap::convert::TryTo;
use twmap::*;

use clap::Parser;

use std::collections::HashMap;
use std::path::PathBuf;

#[derive(Parser, Debug)]
#[clap(author, version)]
#[clap(
    about = "Parses maps and report errors/oddities depending on the verbosity level set. If there are multiple maps passed, the map file name will be printed at the start of each line"
)]
struct Cli {
    #[clap(required = true, value_parser)]
    maps: Vec<PathBuf>,
    /// Check for faulty hookthrough: combo ht with no solid block underneath
    #[clap(long, value_parser)]
    hookthrough: bool,
    /// Check for direction sensitive blocks in the physics layer which have an invalid orientation
    #[clap(long, value_parser)]
    directional_blocks: bool,
    /// Checks for teleporter tiles with the number 0 or with a missing matching entry/exit
    #[clap(long, value_parser)]
    faulty_tele: bool,
    /// Checks if tile ids are actually valid (defined by code)
    #[clap(long, value_parser)]
    tile_ids: bool,
    /// Checks for irregular map settings
    #[clap(long, value_parser)]
    map_settings: bool,
    /// Checks if time cps are placed in the map
    #[clap(long, value_parser)]
    time_cps: bool,
    /// If only one map is passed, still write it's file path in front of lines
    #[clap(long, value_parser)]
    always_file_path: bool,
    /// Print a short summary at the end
    #[clap(long, value_parser)]
    summary: bool,
}

impl Cli {
    fn checks(&mut self) -> [&mut bool; 6] {
        [
            &mut self.hookthrough,
            &mut self.directional_blocks,
            &mut self.faulty_tele,
            &mut self.tile_ids,
            &mut self.map_settings,
            &mut self.time_cps,
        ]
    }
}

fn main() {
    let mut cli: Cli = Cli::parse();
    if cli.checks().into_iter().all(|b| !*b) {
        cli.checks().into_iter().for_each(|x| *x = true);
    }
    let print_file_paths = cli.maps.len() > 1 || cli.always_file_path;

    let mut ht_results = HookthroughSummary::default();
    let mut directional_sensitive_results = HashMap::<u8, u64>::default();
    let mut total_faulty_teles = 0;
    let mut total_invalid_tile_ids = 0;
    let mut total_missing_time_cps: u64 = 0;
    let mut total_irregular_map_settings: u64 = 0;

    for file_path in &cli.maps {
        let print_path = print_file_paths.then_some(file_path);
        let mut map = match TwMap::parse_path(file_path) {
            Err(err) => {
                stderr_maybe_print_path(print_path);
                eprintln!("{}", err);
                continue;
            }
            Ok(map) => map,
        };
        if cli.hookthrough {
            if let Err(err) = map_check_hookthrough(&mut map, print_path, &mut ht_results) {
                stderr_maybe_print_path(print_path);
                eprintln!("{}", err);
            }
        }
        if cli.directional_blocks {
            if let Err(err) = map_check_direction_sensitive(
                &mut map,
                print_path,
                &mut directional_sensitive_results,
            ) {
                stderr_maybe_print_path(print_path);
                eprintln!("{}", err);
            }
        }
        if cli.faulty_tele {
            match map_check_tele_tiles(&mut map, print_path) {
                Ok(n) => total_faulty_teles += n,
                Err(err) => {
                    stderr_maybe_print_path(print_path);
                    eprintln!("{}", err);
                }
            }
        }
        if cli.tile_ids {
            match map_check_tile_ids(&mut map, print_path) {
                Ok(n) => total_invalid_tile_ids += n,
                Err(err) => {
                    stderr_maybe_print_path(print_path);
                    eprintln!("{}", err);
                }
            }
        }
        if cli.time_cps {
            match map_has_time_cps(&mut map) {
                Ok(false) => {
                    maybe_print_path(print_path);
                    println!("Time cps are missing on the map");
                    total_missing_time_cps += 1;
                }
                Ok(true) => {}
                Err(err) => {
                    stderr_maybe_print_path(print_path);
                    eprintln!("{}", err);
                }
            }
        }
        if cli.map_settings {
            total_irregular_map_settings += map_check_settings(&map, print_path);
        }
    }
    if cli.summary {
        println!("\nSummary:");
        if cli.hookthrough {
            println!(
                "Hookthrough combo (id 5) without blocks (faulty): {}",
                ht_results.faulty
            );
            println!(
                "Correctly placed hookthrough combo blocks: {}",
                ht_results.combo
            );
            println!(
                "Hookthrough tiles (id 66) with block underneath (no hookblock): {}.",
                ht_results.new
            );
        }
        if cli.directional_blocks {
            for (id, num) in directional_sensitive_results {
                if num != 0 {
                    println!("Mirrored {} (id {}) count: {}", sensitive_id(id), id, num);
                }
            }
        }
        if cli.faulty_tele {
            println!("Faulty tele tiles: {}", total_faulty_teles);
        }
        if cli.tile_ids {
            println!("Tiles with an invalid id: {}", total_invalid_tile_ids);
        }
        if cli.map_settings {
            println!("Irregular map settings: {}", total_irregular_map_settings)
        }
        if cli.time_cps {
            println!("Maps without time cps: {}", total_missing_time_cps);
        }
    }
}

fn maybe_print_path(path: Option<&PathBuf>) {
    if let Some(path) = path {
        print!("{:?}: ", path);
    }
}

#[derive(Default)]
struct HookthroughSummary {
    faulty: u64,
    combo: u64,
    new: u64,
}

fn map_check_hookthrough(
    map: &mut TwMap,
    path: Option<&PathBuf>,
    results: &mut HookthroughSummary,
) -> Result<(), MapError> {
    if map.find_physics_layer::<FrontLayer>().is_none() {
        return Ok(());
    }
    map.groups
        .load_conditionally(|layer| [LayerKind::Game, LayerKind::Front].contains(&layer.kind()))?;

    let game_tiles = map
        .find_physics_layer::<GameLayer>()
        .unwrap()
        .tiles
        .unwrap_ref();
    let front_tiles = map
        .find_physics_layer::<FrontLayer>()
        .unwrap()
        .tiles
        .unwrap_ref();

    results.faulty += game_tiles
        .indexed_iter()
        .zip(front_tiles.iter())
        .filter(|((_, game), front)| front.id == 5 && (game.id != 1 && game.id != 3))
        .map(|(((y, x), _), _)| {
            maybe_print_path(path);
            println!(
                "x: {}, y: {} -> old hookthrough without a block underneath",
                x, y
            );
        })
        .count() as u64;
    results.combo += game_tiles
        .iter()
        .zip(front_tiles.iter())
        .filter(|(game, front)| front.id == 5 && (game.id == 1 || game.id == 3))
        .count() as u64;
    results.new += game_tiles
        .iter()
        .zip(front_tiles.iter())
        .filter(|(game, front)| front.id == 66 && (game.id == 1 || game.id == 3))
        .count() as u64;
    Ok(())
}

fn check_direction_sensitive_tile_map<T: TilemapLayer>(
    layer: &T,
    results: &mut HashMap<u8, u64>,
    path: Option<&PathBuf>,
) -> Result<(), MapError> {
    layer
        .tiles()
        .unwrap_ref()
        .indexed_iter()
        .for_each(|((y, x), tile)| {
            let id = tile.id();
            let flags = tile.flags().unwrap();
            if is_sensitive(id)
                && (flags.contains(TileFlags::FLIP_Y) ^ flags.contains(TileFlags::FLIP_X))
            {
                *results.entry(id).or_insert(0) += 1;
                maybe_print_path(path);
                println!("x: {}, y: {} -> {} is mirrored", x, y, sensitive_id(id));
            }
        });
    Ok(())
}

fn map_check_direction_sensitive(
    map: &mut TwMap,
    path: Option<&PathBuf>,
    results: &mut HashMap<u8, u64>,
) -> Result<(), MapError> {
    map.groups.load_conditionally(|layer| {
        [LayerKind::Game, LayerKind::Front, LayerKind::Switch].contains(&layer.kind())
    })?;
    check_direction_sensitive_tile_map(
        map.find_physics_layer::<GameLayer>().unwrap(),
        results,
        path,
    )?;
    if let Some(front_layer) = map.find_physics_layer::<FrontLayer>() {
        check_direction_sensitive_tile_map(front_layer, results, path)?;
    }
    if let Some(switch_layer) = map.find_physics_layer::<SwitchLayer>() {
        check_direction_sensitive_tile_map(switch_layer, results, path)?;
    }
    Ok(())
}

const fn is_sensitive(id: u8) -> bool {
    matches!(id, 60 | 61 | 64 | 65 | 67 | 203..=209 | 224 | 225)
}

fn sensitive_id(id: u8) -> &'static str {
    match id {
        60 => "stopper",
        61 => "bi-directional stopper",
        64 => "arrow/speeder",
        65 => "double arrow/fast speeder",
        67 => "directional hookthrough",
        203..=209 => "spinning freezing laser",
        224 => "exploding bullet",
        225 => "freezing bullet",
        _ => unreachable!(),
    }
}

fn is_from_tele(id: u8) -> bool {
    matches!(id, 10 | 14 | 15 | 26)
}

fn is_useless_alone_from_tele(id: u8) -> bool {
    matches!(id, 10 | 26)
}

fn is_to_tele(id: u8) -> bool {
    matches!(id, 27)
}

const TELE_CP: u8 = 29;
const CP_TELE_TO: u8 = 30;

fn is_cp_tele_from(id: u8) -> bool {
    matches!(id, 31 | 63)
}

fn tele_error(
    tile: &str,
    err: &str,
    tele: Tele,
    yx: &(usize, usize),
    path: Option<&PathBuf>,
) -> bool {
    maybe_print_path(path);
    if is_cp_tele_from(tele.id) {
        println!("{} at x: {}, y: {} {}", tile, yx.1, yx.0, err);
    } else {
        println!(
            "{} (number {}) at x: {}, y: {} {}",
            tile, tele.number, yx.1, yx.0, err
        );
    }
    true
}

fn map_check_tele_tiles(map: &mut TwMap, path: Option<&PathBuf>) -> Result<u64, MapError> {
    map.groups
        .load_conditionally(|layer| layer.kind() == LayerKind::Tele)?;
    let tele_tiles = match map.find_physics_layer::<TeleLayer>() {
        None => return Ok(0),
        Some(tele_layer) => tele_layer.tiles.unwrap_ref(),
    };
    let mut used_from_teles = [false; 256];
    let mut used_to_teles = [false; 256];
    let mut used_tele_cps = [false; 256];
    let mut used_cp_to_teles = [false; 256];
    let mut exists_cp_from_tele = false;
    tele_tiles.iter().for_each(|tele| {
        let number = usize::from(tele.number);
        if is_from_tele(tele.id) {
            used_from_teles[number] = true;
        } else if is_to_tele(tele.id) {
            used_to_teles[number] = true;
        } else if tele.id == TELE_CP {
            used_tele_cps[number] = true;
        } else if tele.id == CP_TELE_TO {
            used_cp_to_teles[number] = true;
        } else if is_cp_tele_from(tele.id) {
            exists_cp_from_tele = true;
        }
    });

    let count = tele_tiles
        .indexed_iter()
        .filter(|(_, tele)| !(tele.id == 0 && tele.number == 0))
        .filter(|(yx, &tele)| {
            let number = usize::from(tele.number);
            if tele.number == 0 {
                let err = "shouldn't (like any other tele tile) have the (disabling) number 0";
                tele_error("Tele tile", err, tele, yx, path)
            } else if is_useless_alone_from_tele(tele.id) && !used_to_teles[number] {
                let err = "doesn't have a matching To-Tele";
                tele_error("From-Tele", err, tele, yx, path)
            } else if is_to_tele(tele.id) && !used_from_teles[number] {
                let err = "doesn't have a matching From-Tele";
                tele_error("To-Tele", err, tele, yx, path)
            } else if tele.id == CP_TELE_TO && !exists_cp_from_tele {
                tele_error("Cp-To-Tele", "without any Cp-From-Tele", tele, yx, path)
            } else if tele.id == TELE_CP && !used_cp_to_teles[number] {
                let err = "doesn't have a matching Cp-To-Tele";
                tele_error("Tele-CP", err, tele, yx, path)
            } else if tele.id == CP_TELE_TO && !used_tele_cps[number] {
                let err = "doesn't have a matching Tele-CP";
                tele_error("Cp-To-Tele", err, tele, yx, path)
            } else {
                false
            }
        })
        .count();
    Ok(count.try_to())
}

fn stderr_maybe_print_path(path: Option<&PathBuf>) {
    if let Some(path) = path {
        eprint!("{:?}: ", path);
    }
}

fn is_valid_entity_tile(id: u8) -> bool {
    matches!(id, 192..=218 | 220..=229 | 233..=238 | 240)
}

fn is_valid_game_or_front_tile(id: u8) -> bool {
    matches!(id,
        2
        | 4..=6
        | 9
        | 11..=13
        | 16..=22
        | 32..=62
        | 64..=67
        | 71..=76
        | 88..=91
        | 96..=97
        | 104..=107
        | 112..=113
        | 128..=129
        | 144..=145
        | 190..=191
    ) || is_valid_entity_tile(id)
}

fn is_valid_game_tile(id: u8) -> bool {
    matches!(id, 1 | 3) || is_valid_game_or_front_tile(id)
}

fn is_valid_front_tile(id: u8) -> bool {
    matches!(id, 98..=99) || is_valid_game_or_front_tile(id)
}

fn is_valid_tele_tile(id: u8) -> bool {
    matches!(id, 10 | 14..=15 | 26..=27 | 29..=31 | 63)
}

fn is_valid_speedup_tile(id: u8) -> bool {
    id == 28
}

fn is_valid_switch_tile(id: u8) -> bool {
    match id {
        7 | 9 | 12..=13 | 19..=20 | 22..=25 | 79 | 95 | 98..=99 | 144..=145 => true,
        _ if id >= 197 => is_valid_entity_tile(id),
        _ => false,
    }
}

fn is_valid_tune_tile(id: u8) -> bool {
    id == 68
}

macro_rules! check_invalid_tiles {
    ($layer: ident, $kind: ident, $path: ident, $check_fn: ident) => {{
        let tiles = $layer.tiles.unwrap_ref();
        tiles
            .indexed_iter()
            .filter(|(_, tile)| tile.id != 0 && !$check_fn(tile.id))
            .map(|((y, x), tile)| {
                maybe_print_path($path);
                println!(
                    "{:?} tile at x: {}, y: {} with an invalid id: {}",
                    $kind, x, y, tile.id
                )
            })
            .count() as u64
    }};
}

fn check_tile_ids(layer: &Layer, path: Option<&PathBuf>) -> u64 {
    let kind = layer.kind();
    match layer {
        Layer::Game(layer) => check_invalid_tiles!(layer, kind, path, is_valid_game_tile),
        Layer::Front(layer) => check_invalid_tiles!(layer, kind, path, is_valid_front_tile),
        Layer::Tele(layer) => check_invalid_tiles!(layer, kind, path, is_valid_tele_tile),
        Layer::Speedup(layer) => check_invalid_tiles!(layer, kind, path, is_valid_speedup_tile),
        Layer::Switch(layer) => check_invalid_tiles!(layer, kind, path, is_valid_switch_tile),
        Layer::Tune(layer) => check_invalid_tiles!(layer, kind, path, is_valid_tune_tile),
        _ => 0,
    }
}

fn map_check_tile_ids(map: &mut TwMap, path: Option<&PathBuf>) -> Result<u64, MapError> {
    map.groups
        .load_conditionally(|layer| layer.kind().is_physics_layer())?;
    let mut total = 0;
    for group in &map.groups {
        for layer in &group.layers {
            total += check_tile_ids(layer, path);
        }
    }
    Ok(total)
}

const UNDECIDED: &str = "It is undecided if this setting should be used in DDNet";
const UNUSUAL: &str = "Unusual";
const DEPRECATED: &str = "Enables deprecated behavior";
const DEPRECATED_BUGGY: &str = "Enables deprecated buggy behavior";
const REPLACED: &str = "Replaced by a tile";

const IRREGULAR_MAP_SETTINGS: [(&str, &str); 17] = [
    ("sv_no_weak_hook_and_bounce", UNDECIDED),
    ("sv_old_laser", DEPRECATED),
    ("sv_old_teleport_weapons", DEPRECATED),
    ("sv_old_teleport_hook", DEPRECATED),
    ("mapbug", DEPRECATED_BUGGY),
    ("sv_reset_pickups", DEPRECATED),
    ("sv_save_worse_scores", UNUSUAL),
    ("sv_show_others_default", UNUSUAL),
    ("sv_teleport_lose_weapons", UNUSUAL),
    ("sv_teleport_hold_hook", UNUSUAL),
    ("sv_destroy_bullets_on_death", UNUSUAL),
    ("sv_destroy_lasers_on_death", UNUSUAL),
    ("sv_plasma_range", UNUSUAL),
    ("sv_plasma_per_sec", UNUSUAL),
    ("sv_dragger_range", UNUSUAL),
    ("sv_endless_drag", REPLACED),
    ("sv_hit", REPLACED),
];

fn map_check_settings(map: &TwMap, path: Option<&PathBuf>) -> u64 {
    let mut irregular_map_settings = 0;
    for setting in &map.info.settings {
        for (irregular_setting, reason) in IRREGULAR_MAP_SETTINGS {
            if setting.contains(irregular_setting) {
                maybe_print_path(path);
                println!("Map setting - {}: '{}'", reason, irregular_setting);
                irregular_map_settings += 1;
            }
        }
    }
    irregular_map_settings
}

fn map_has_time_cps(map: &mut TwMap) -> Result<bool, MapError> {
    map.groups.load_conditionally(|layer| {
        layer.kind() == LayerKind::Game || layer.kind() == LayerKind::Front
    })?;
    let game_tiles = map
        .find_physics_layer::<GameLayer>()
        .unwrap()
        .tiles
        .unwrap_ref();
    let front_tiles = map
        .find_physics_layer::<FrontLayer>()
        .map(|layer| layer.tiles.unwrap_ref());
    Ok(game_tiles.iter().any(|tile| (35..60).contains(&tile.id))
        || front_tiles
            .map(|tiles| tiles.iter().any(|tile| (35..60).contains(&tile.id)))
            .unwrap_or(false))
}
